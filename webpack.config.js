const path = require('path');
var webpack = require('webpack');


module.exports = {
 entry: './assets/js/index.js',
  output: {
    filename: 'script.min.js',
    path: path.resolve(__dirname, './assets/js/')
  },
  watch: true,
  module: {
      rules:  [
          {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                      'scss': 'vue-style-loader!css-loader!sass-loader',
                      'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                    }
                  }
          },
         
      ]
      
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.common.js'
    }
  },
};